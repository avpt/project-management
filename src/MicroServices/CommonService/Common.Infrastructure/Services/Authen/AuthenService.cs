﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using Common.Application.Interfaces;
using Common.Domain.Models;
using Core.Domain.Constants;
using Core.Domain.Entities;
using Core.Domain.Enums;
using Core.Domain.I18N;
using Core.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Common.Infrastructure.Services
{
    public class AuthenService : IAuthenService
    {
        private readonly JWTSetting _jWtSetting;
        private readonly UserManager<UserIdentity> _userManager;
        private readonly SignInManager<UserIdentity> _signInManager;
        public AuthenService(IOptionsMonitor<JWTSetting> optionsMonitor,
            UserManager<UserIdentity> userManager,
            SignInManager<UserIdentity> signInManager)
        {
            _jWtSetting = optionsMonitor.CurrentValue;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public async Task<string> GenerateToken(UserIdentity user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var secertKeyBytes = Encoding.UTF8.GetBytes(_jWtSetting.SecertKey);
            var roles = await _userManager.GetRolesAsync(user);
            string currentRole = roles[0];
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(CustomClaimTypes.Role, currentRole),
                    new Claim(CustomClaimTypes.Email, user.Email ?? ""),
                    new Claim(CustomClaimTypes.UserId, user.Id)
                }),
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secertKeyBytes), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = jwtTokenHandler.CreateToken(tokenDescription);
            var accessToken = jwtTokenHandler.WriteToken(token);
            return accessToken;
        }

        public Task<string> GetUserEmailById(Guid userId)
        {
            throw new NotImplementedException();
        }

        public async Task<APIResponseModel> Login(LoginModel loginModel)
        {
            APIResponseModel result = new APIResponseModel()
            {
                Message = "Login successfully",
                Data = loginModel,
                IsSuccess = true,
                StatusCode = (int)HttpStatusCode.OK
            };
            var user = await _userManager.FindByEmailAsync(loginModel.Email);
            if (user != null && user.Status != UserStatus.Active)
            {
                return new APIResponseModel()
                {
                    Message = "your account has been no longer to access the system!",
                    Data = loginModel,
                    IsSuccess = false,
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            if (user == null)
            {
                return new APIResponseModel()
                {
                    Message = CoreI18NEntity.GetString("Core_Test_Message"),
                    Data = loginModel,
                    IsSuccess = false,
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            var loginResult = await _signInManager.CheckPasswordSignInAsync(user, loginModel.Password, lockoutOnFailure: true);
            if (loginResult.Succeeded)
            {
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Message = "Login successfully!";
                result.IsSuccess = true;
                result.Data = await GenerateToken(user);
            }
            else if (loginResult.IsLockedOut)
            {
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                result.Message = "your account has been locked, please wait for 30 minutes";
                result.IsSuccess = false;
                result.Data = loginModel;
            }
            else
            {
                result.StatusCode = (int)HttpStatusCode.BadRequest;
                result.Message = "your email or password is incorrect, please try again!";
                result.IsSuccess = false;
                result.Data = loginModel;
            }
            return result;
        }
    }
}
