﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Domain.Models;
using Core.Domain.Entities;
using Core.Domain.Models;

namespace Common.Application.Interfaces;
public interface IAuthenService
{
    Task<APIResponseModel> Login(LoginModel loginModel);
    Task<string> GenerateToken(UserIdentity user);
    Task<string> GetUserEmailById(Guid userId);
}
