﻿using System.Text;
using Common.Application.Interfaces;
using Common.Domain.Models;
using Core.Application;
using Core.Application.Attributes.Roles;
using Core.Domain.Constants;
using Core.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace Common.API.Controllers.Authen;

public class AuthenController : APIBaseController
{
    private readonly IAuthenService _authenService;
    private readonly IDistributedCache _redisCache;
    public AuthenController(IHttpContextAccessor contextAccessor,
        IAuthenService authenService,
        IDistributedCache redisCache) : base(contextAccessor)
    {
        _authenService = authenService;
        _redisCache = redisCache;
    }

    [HttpPost("login")]
    public async Task<APIResponseModel> Login([FromBody] LoginModel loginModel)
    {
        try
        {
            var result = await _authenService.Login(loginModel);
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new APIResponseModel()
            {
                Message = ex.Message,
                Data = ex,
                IsSuccess = false,
                StatusCode = StatusCodes.Status409Conflict
            };
        }
    }

    [HttpGet("test/cache")]
    [CustomRole(CustomClaimTypes.Role, UserRoles.SuperAdmin)]
    public async Task<APIResponseModel> TestCache()
    {
        try
        {
            var result = new APIResponseModel()
            {
                Message = "Cache test",
                IsSuccess = true,
                StatusCode = StatusCodes.Status200OK
            };
            await _redisCache.SetStringAsync("test_key", "value ne", new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(30)
            });
            var cacheValue = await _redisCache.GetAsync("test_key");
            result.Data = cacheValue;
            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new APIResponseModel()
            {
                Message = ex.Message,
                Data = ex,
                IsSuccess = false,
                StatusCode = StatusCodes.Status409Conflict
            };
        }
    }

}
