﻿using Core.Domain.Enums;
using Core.Application.Extensions;
using Core.Application.BuilderUpgrades.Interfaces;
using Core.Application.BuilderUpgrades;
using Core.Domain.Constants;
using Core.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Common.Application.Interfaces;
using Common.Infrastructure.Services;

namespace Common.API;

public class Startup
{
    public IConfiguration Configuration { get; }
    public Startup(IConfiguration configuration)
    {
        this.Configuration = configuration;
    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddBasicConfigure();
        services.AddDbContext<CoreDbContext>(Configuration.GetConnectionString(ConfigurationKeys.CoreDbConnectionString)
            ?? throw new ArgumentNullException(nameof(ConfigurationKeys.CoreDbConnectionString)));
        services.AddJWTAuthen(Configuration);

        services.AddTransient<IDataBuilderService, DataBuilderService>();
        services.AddTransient<IAuthenService, AuthenService>();
        services.AddRedisCache(Configuration);
    }
    public void Configure(WebApplication app, IWebHostEnvironment env)
    {
        app.UseBasicConfigure(env, MicroServiceComponent.Common);
        var scope = app.Services.CreateScope();
        var dataBuilderService = scope.ServiceProvider.GetService<IDataBuilderService>();
        if (dataBuilderService != null)
        {
            dataBuilderService.RunAsync().GetAwaiter().GetResult();
        }
    }
}
