﻿using System;
using System.Globalization;
using System.Text;
using AutoMapper;
using Common.Domain.Models;
using Core.Domain.Constants;
using Core.Domain.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace Core.Application.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddBasicConfigure(this IServiceCollection services)
        {
            services.AddControllers().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddCors(p => p.AddDefaultPolicy(build =>
            {
                build.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            }));
            services.AddHealthChecks();
            services.AddHttpContextAccessor();
            services.AddI18NLocalization();
            return services;
        }
        public static IServiceCollection AddAutoMapperConfig(this IServiceCollection services, Type startupType, Profile profile)
        {
            try
            {
                services.AddAutoMapper(startupType);
                var mapperConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(profile);
                });
                IMapper mapper = mapperConfig.CreateMapper();
                services.AddSingleton(mapper);
            }
            catch (Exception ex)
            {
                throw new Exception($"Error when add auto mapper with error: {ex}");
            }
            return services;
        }
        public static IServiceCollection AddDbContext<TContext>(this IServiceCollection services, string connectionStr)
            where TContext : DbContext
        {
            services.AddIdentity<UserIdentity, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
                options.User.RequireUniqueEmail = true;
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.AllowedForNewUsers = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
            }).AddEntityFrameworkStores<TContext>()
           .AddDefaultTokenProviders();

            services.AddIdentityCore<UserIdentity>();
            services.AddDbContext<TContext>(options =>
            {
                options.UseNpgsql(connectionStr);
            });
            return services;
        }
        public static IServiceCollection AddJWTAuthen(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c =>
            {
                var securityScheme = new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Bearer token for JWT Authorization",
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = JwtBearerDefaults.AuthenticationScheme
                    }
                };
                c.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, securityScheme);

                var securityRequirement = new OpenApiSecurityRequirement
                {
                    {
                        securityScheme,
                        new string[] {}
                    }
                };
                c.AddSecurityRequirement(securityRequirement);
            });

            services.Configure<JWTSetting>(configuration.GetSection(ConfigurationKeys.JWTSetting));
            var secertKey = configuration[ConfigurationKeys.JWTSettingSecretKey];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secertKey 
                ?? throw new ArgumentNullException(nameof(ConfigurationKeys.JWTSettingSecretKey)));
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secretKeyBytes),
                    ClockSkew = TimeSpan.Zero,
                };
            });
            return services;
        }

        public static IServiceCollection AddRedisCache(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = configuration.GetValue<string>(ConfigurationKeys.CacheSettingKey);
            });
            return services;
        }

        public static IServiceCollection AddI18NLocalization(this IServiceCollection services)
        {
            services.AddLocalization((options => options.ResourcesPath = "I18N"));
            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en-us"),
                        new CultureInfo("vi-VN")
                    };
                    options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;
                    options.RequestCultureProviders = new[]
                    {
                        new RouteDataRequestCultureProvider()
                    };
                });
            return services;
        }
        
    }
}

