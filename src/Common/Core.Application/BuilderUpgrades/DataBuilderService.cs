﻿using System;
using Core.Application.BuilderUpgrades.Interfaces;
using Core.Domain.Constants;
using Core.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace Core.Application.BuilderUpgrades;
public class DataBuilderService : IDataBuilderService
{
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly UserManager<UserIdentity> _userManager;
    public DataBuilderService(RoleManager<IdentityRole> roleManager, UserManager<UserIdentity> userManager)
    {
        _roleManager = roleManager;
        _userManager = userManager;
    }

    public async Task RunAsync()
    {
        await CreateRolesDefaultAsync();
        await AddSuperAdminDefaultAsync();
    }

    private async Task CreateRolesDefaultAsync()
    {
        try
        {
            foreach (var role in UserRoles.Roles)
            {
                var existRole = await _roleManager.RoleExistsAsync(role);
                if (!existRole)
                {
                    await _roleManager.CreateAsync(new IdentityRole(role));
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        await Task.CompletedTask;
    }

    private async Task AddSuperAdminDefaultAsync()
    {
        try
        {
            var userEntity = await _userManager.FindByIdAsync(Constants.SuperAdminId);
            if (userEntity == null)
            {
                userEntity = new UserIdentity()
                {
                    Id = Constants.SuperAdminId,
                    FullName = Constants.SuperAdminFullName,
                    Email = Constants.SuperAdminEmail,
                    CreatedDate = DateTime.Now.Ticks,
                    Gender = Domain.Enums.UserGender.Unknown,
                    PhoneNumber = "0000000000",
                    DateOfBirth = DateTime.Now.Ticks,
                    Status = Domain.Enums.UserStatus.Active,
                    UpdatedDate = DateTime.Now.Ticks,
                    UserName = Constants.SuperAdminUserName
                };
                var resultAdd = await _userManager.CreateAsync(userEntity, Constants.SuperAdminPassword);
                if (resultAdd.Succeeded)
                {
                    await _userManager.AddToRoleAsync(userEntity, UserRoles.SuperAdmin);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        await Task.CompletedTask;
    }

}
