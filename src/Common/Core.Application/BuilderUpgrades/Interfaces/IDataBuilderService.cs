﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Application.BuilderUpgrades.Interfaces;
public interface IDataBuilderService
{
    Task RunAsync();
}
