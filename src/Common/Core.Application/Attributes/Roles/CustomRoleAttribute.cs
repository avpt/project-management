﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Core.Application.Attributes.Roles;
public class CustomRoleAttribute : TypeFilterAttribute
{
    public CustomRoleAttribute(string claimType, string claimValue) : base(typeof(RolesFilter))
    {
        Arguments = new object[] { new Claim(claimType, claimValue) };
    }
}

public class RolesFilter : IAuthorizationFilter
{
    readonly Claim _claim;

    public RolesFilter(Claim claim)
    {
        _claim = claim;
    }

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        if (!context.HttpContext.User.Identity.IsAuthenticated)
        {
            context.Result = new UnauthorizedResult();
            return;
        }
        var hasClaim = context.HttpContext.User.Claims.Any(c => c.Type == _claim.Type && c.Value == _claim.Value);
        if (!hasClaim)
        {
            context.Result = new ForbidResult();
        }
    }
}
