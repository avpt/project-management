using System.Globalization;
using System.Reflection;
using System.Resources;
using Core.Domain.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Domain.I18N;

public static class I18NBase
{
    private static readonly Dictionary<string, ResourceManager> _ResourceManagers =
        new Dictionary<string, ResourceManager>();

    private static ResourceManager GetResourceManager(string baseName, Assembly assembly)
    {
        if (string.IsNullOrEmpty(baseName))
        {
            throw new ArgumentNullException(nameof(baseName));
        }

        lock (_ResourceManagers)
        {
            if (!_ResourceManagers.ContainsKey(baseName))
            {
                var resourceBaseName = baseName;
                var assemblyTypes = assembly.GetTypes();
                var programType = assemblyTypes.FirstOrDefault(s =>
                    s.Name.Equals("I18NEntity", StringComparison.OrdinalIgnoreCase)
                    || s.Name.Equals("CoreI18NEntity", StringComparison.OrdinalIgnoreCase));
                if (programType != null)
                {
                    resourceBaseName = programType.Namespace + "." + baseName;
                }
                _ResourceManagers.Add(baseName, new ResourceManager(resourceBaseName, assembly));
            }
        }
        return _ResourceManagers[baseName];
    }

    public static string GetString(Assembly assembly, string baseName, string key, CultureInfo cultureInfo,
        params object[] args)
    {
        try
        {
            var _resourceManager = GetResourceManager(baseName, assembly);
            if (cultureInfo == null)
            {
                cultureInfo = new CultureInfo("en-US");
            }

            string text = _resourceManager.GetString(key, cultureInfo);
            bool flag = args == null || args.Length == 0;
            if (!string.IsNullOrEmpty(text))
            {
                bool isShowI18N = false;
                if (RunningContext.ServiceProvider != null)
                {
                    var httpContext = RunningContext.ServiceProvider.GetService<IHttpContextAccessor>();
                    isShowI18N = !string.IsNullOrEmpty(httpContext?.HttpContext?.Request?.Headers["testi18n"]);
                }

                if (!flag)
                {
                    return string.Format(text, args);
                }

                return isShowI18N ? "^" + text + "^" : text;
            }
            else
            {
                if (!flag)
                {
                    return string.Format(key, args);
                }
                return key;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return key;
        }
    }
}
