using System.Globalization;
using System.Reflection;
using Core.Domain.Constants;

namespace Core.Domain.I18N;

public static class CoreI18NEntity
{
    public static string GetString(string key, CultureInfo culture = null)
    {
        var assemble = Assembly.GetExecutingAssembly();
        return I18NBase.GetString(assemble, I18NBaseNameConstants.I18NBase, key, culture, null);
    }
}
