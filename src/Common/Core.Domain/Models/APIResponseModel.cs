﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Models;
public class APIResponseModel
{
    public int StatusCode { get; set; }
    public required string Message { get; set; }
    public object? Data { get; set; }
    public bool IsSuccess { get; set; }
}
