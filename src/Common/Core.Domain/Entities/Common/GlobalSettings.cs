﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Enums;

namespace Core.Domain.Entities;
public class GlobalSettings : BaseEntity
{
    public required string Details { get; set; }
    public GlobalSettingType Type { get; set; }
}
