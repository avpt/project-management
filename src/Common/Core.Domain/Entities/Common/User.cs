﻿using System;
using Core.Domain.Enums;
using Microsoft.AspNetCore.Identity;

namespace Core.Domain.Entities
{
    public class UserIdentity : IdentityUser
    {
        public long DateOfBirth { get; set; }
        public required string FullName { get; set; }
        public UserStatus Status { get; set; }
        public UserGender Gender { get; set; }
        public long CreatedDate { get; set; }
        public long UpdatedDate { get; set; }
    }
}
