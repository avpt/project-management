﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Constants;
using Core.Domain.Enums;

namespace Core.Domain.Utility;
public static class CommonUtility
{
    public static string GetAPINameByComponent(MicroServiceComponent component)
    {
        string result = "";
        switch (component)
        {
            case MicroServiceComponent.Gateway:
                result = APIName.Gateway;
                break;
            case MicroServiceComponent.Common:
                result = APIName.Common;
                break;
            case MicroServiceComponent.Project:
                result = APIName.Project;
                break;
            default:
                break;
        }
        return result;
    }
}
