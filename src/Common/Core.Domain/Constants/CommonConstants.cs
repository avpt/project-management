﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Constants
{
    public static class Constants
    {
        public const string SuperAdminId = "4CED47E2-F549-40EC-AA42-1E2DF140ADA8";
        public const string SuperAdminPassword = "Superadmin123@";
        public const string SuperAdminFullName = "Super Admin";
        public const string SuperAdminEmail = "superAdmin@gmail.com";
        public const string SuperAdminUserName = "superAdmin";
    }

    public static class ConfigurationKeys
    {
        public const string MicroserviceEndpoints = "MicroserviceEndpoints";
        public const string MicroserviceEndpointsAPIGW = "MicroserviceEndpointsAPIGW";
        public const string CoreDbConnectionString = "CoreDbConnectStr";
        public const string JWTSetting = "JWTSetting";
        public const string JWTSettingSecretKey = "JWTSetting:SecertKey";
        public const string CacheSettingKey = "CacheSettings:RedisCache";
    }

    public static class MicroServicesPorts
    {
        public const int Common = 15001;
        public const int Project = 15002;
    }

    public static class ApplicationHostName
    {
        public const string APIGateWay = "API Gateway";
        public const string Common = "Common Service";
        public const string Project = "Project Service";
    }

    public static class APIName
    {
        public const string Gateway = "Gateway API";
        public const string Common = "Common API";
        public const string Project = "Project API";
    }

    public static class TableNameConstant
    {
        public const string GlobalSettingTable = "CGlobalSettings";
        public const string DepartmentTable = "CDepartments";
    }

    public static class TableIndexName
    {
        public const string PK_GlobalSettingTable = "PK_CGlobalSettings";
    }

    public static class UserRoles
    {
        public const string SuperAdmin = "SuperAdmin";
        public const string Admin = "Admin";
        public const string Manager = "Manager";
        public const string Leader = "Leader";
        public const string Employee = "Employee";

        public static ImmutableList<string> Roles =
            ImmutableList.Create<string>(SuperAdmin, Admin, Manager, Leader, Employee);
    }

    public static class CustomClaimTypes
    {
        public const string Role = ClaimTypes.Role;
        public const string Email = ClaimTypes.Email;
        public const string UserId = "UserId";
    }

    public static class RunningContext
    {
        public static IServiceProvider ServiceProvider { get; set; }
    }

    public static class I18NBaseNameConstants
    {
        public static string I18NBase = "coreI18N.en-us";
    }
}
