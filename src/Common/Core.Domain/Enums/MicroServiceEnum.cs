﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Enums
{
    public enum MicroServiceComponent : byte
    {
        Gateway = 0,
        Common = 1,
        Project = 2
    }
}

