﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Enums;
public enum GlobalSettingType: byte
{
    SMTP = 0,
    FileUpload = 1,
}
