﻿using System;

namespace Core.Domain.Enums
{
    public enum UserStatus : byte
    {
        Active = 0,
        Inactive = 1,
        Delete = 2
    }
    public enum UserGender : byte
    {
        Unknown = 0,
        Male = 1,
        Female = 2,
    }
    public enum UserRoles : byte
    {
        SuperAdmin = 0,
        Admin = 1,
        Manager = 2,
        Leader = 3,
        Employee = 4
    }
}
