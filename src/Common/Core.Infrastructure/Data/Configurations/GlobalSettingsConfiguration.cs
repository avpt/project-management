﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain.Constants;
using Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Infrastructure.Data.Configurations;
internal class GlobalSettingsConfiguration : IEntityTypeConfiguration<GlobalSettings>
{
    public void Configure(EntityTypeBuilder<GlobalSettings> builder)
    {
        builder.ToTable(TableNameConstant.GlobalSettingTable);
        builder.HasKey(g => g.Id).HasName(TableIndexName.PK_GlobalSettingTable);
        builder.Property(g => g.Type).IsRequired();
        builder.Property(g => g.Details).IsRequired();
    }
}
