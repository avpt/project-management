﻿using System;
using Core.Domain.Entities;
using Core.Infrastructure.Data.Configurations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Core.Infrastructure;
public class CoreDbContext : IdentityDbContext<UserIdentity>
{
    public CoreDbContext(DbContextOptions<CoreDbContext> options) : base(options)
    {

    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new GlobalSettingsConfiguration());
        base.OnModelCreating(modelBuilder);
    }

    public DbSet<GlobalSettings> GlobalSettings { get; set; }

}
