// i18n.ts
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import enTranslation from '~/language/locales/en/en.json'; // Import translations for each language
import viTranslation from '~/language/locales/vi/vi.json';
// Add more languages as needed

const resources = {
  en: { translation: enTranslation },
  vi: { translation: viTranslation },
  // Add more languages as needed
};

i18n
  .use(initReactI18next) // pass the i18n instance to react-i18next
  .init({
    resources,
    lng: 'vi', // Default language
    fallbackLng: 'vi', // Fallback language if the current language file is not found
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
