import { useState } from 'react';
import { useTranslation } from 'react-i18next';

function App() {
  const [count, setCount] = useState(0);
  const { t: translation } = useTranslation();
  return (
    <>
      <h1>{translation('Project_Mangament_Entry')}</h1>
      <div>
        <button onClick={() => setCount((count) => count + 1)}>{count}</button>
      </div>
    </>
  );
}

export default App;
