﻿using Core.Application.Extensions;
using Core.Domain.Enums;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

namespace APIGateway;

public class Startup
{
    public IConfiguration Configuration { get; }
    public Startup(IConfiguration configuration)
    {
        this.Configuration = configuration;
    }
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddBasicConfigure();
        services.AddOcelot(new ConfigurationBuilder()
            .AddJsonFile("ocelot.json")
            .Build());
    }
    public void Configure(WebApplication app, IWebHostEnvironment env)
    {
        app.UseBasicConfigure(env, MicroServiceComponent.Gateway);

        app.UseOcelot();

    }
}
